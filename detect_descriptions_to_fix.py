#!/usr/bin/env python

import json

with open('portail.asso-insa-lyon.fr.json') as json_file:
    data = json.load(json_file)
    counter = 0
    for asso in data:
        desc_len = len(asso['description'])
        if (desc_len > 1000 or desc_len < 500) and asso['is_validated'] == True and asso['is_active'] == True:
            counter += 1
            print(asso['name'])
    print(counter)
