#!/usr/bin/env python

import json
import unicodedata
import re
import os
import mimetypes
import requests
import shutil
import itertools
import getpass
import fileinput

categories = [
    {"latex_filename": "latex/animations_et_loisirs.tex",
     "botinsa_category_names": {"Animation du campus", "Loisir"},
     "latex_color": "red",
     "botinsa_ids": {1, 6},
     "placeholder_for_summary_list": "liste-assos-animations"},
    {"latex_filename": "latex/arts_et_spectacles.tex",
     "botinsa_category_names": {"Arts et spectacles"},
     "latex_color": "orange",
     "botinsa_ids": {2},
     "placeholder_for_summary_list": "liste-assos-arts"},
    {"latex_filename": "latex/cultures_internationales.tex",
     "botinsa_category_names": {"Cultures internationales"},
     "latex_color": "yellow",
     "botinsa_ids": {3},
     "placeholder_for_summary_list": "listes-assos-cultures"},
    {"latex_filename": "latex/departements_et_enseignements.tex",
     "botinsa_category_names": {"Association de département"},
     "latex_color": "green",
     "botinsa_ids": {4},
     "placeholder_for_summary_list": "liste-assos-departements"},
    {"latex_filename": "latex/humanitaire_et_social.tex",
     "botinsa_category_names": {"Humanitaire et social"},
     "latex_color": "blue",
     "botinsa_ids": {5},
     "placeholder_for_summary_list": "listes-assos-humanitaire"},
    {"latex_filename": "latex/sport.tex",
     "botinsa_category_names": {"Sport"},
     "latex_color": "purple",
     "botinsa_ids": {7},
     "placeholder_for_summary_list": "listes-assos-sport"},
    {"latex_filename": "latex/techniques_et_scientifique.tex",
     "botinsa_category_names": {"Technique et scientifique"},
     "latex_color": "brick",
     "botinsa_ids": {8},
     "placeholder_for_summary_list": "listes-assos-sciences"}
]

template_intro = """\
\\documentclass{association}

\\justifying

\\begin{document}

"""

template_asso= """\
    \\begin{{association}}
        [{association_acronym}]
        {{{association_name}}}
        [{association_mail}]
        [{association_website}]
        [{association_path_to_logo}]
        [{association_textual_schedule}]

        {association_description}
    \\end{{association}}

"""

template_ending = """
\end{document}
"""

category_color_text = """\
    \\SetCategoryColor{{{color}}}
    \\setlength{{\\parindent}}{{0.5cm}}
    \\setlength{{\\parskip}}{{0.1cm}}
"""

logos_folder = "latex/logos_temp/"
original_main_template_file_path = "latex/template.tex"
main_template_file_path = "latex/main_output_dont_edit_directly.tex"

day_nb_to_name = {1: "Lundi", 2: "Mardi", 3: "Mercredi", 4: "Jeudi", 5: "Vendredi", 6: "Samedi", 7: "Dimanche"}

days_combinations = []
for L in range(8):
    for subset in itertools.combinations([1, 2, 3, 4, 5, 6, 7], L):
        days_combinations.append(subset)
days_combinations = days_combinations[::-1]

def slugify(value, allow_unicode=False):
    """
    Convert to ASCII if 'allow_unicode' is False. Convert spaces to hyphens.
    Remove characters that aren't alphanumerics, underscores, or hyphens.
    Convert to lowercase. Also strip leading and trailing whitespace.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value).strip().lower()
    return re.sub(r'[-\s]+', '-', value)


def is_valid_url(string):
    regex = re.compile(
        r'^(?:http|ftp)s?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)

    return re.match(regex, string) is not None


def is_valid_mail(string):
    regex = re.compile(r"[^@]+@[^@]+\.[^@]+")
    return re.match(regex, string) is not None


def get_logo_path(asso_name, logo_url):
    expected_file_name_base = slugify(asso_name)

    # Make sure the folder exists
    if not os.path.exists(logos_folder):
        os.makedirs(logos_folder)

    # If file has already been downloaded, reuse it
    for filename in os.listdir(logos_folder):
        file_path = os.path.join(logos_folder, filename)
        is_in_logos_folder = os.path.isfile(file_path) and expected_file_name_base == os.path.splitext(filename)[0]
        if is_in_logos_folder:
            return file_path.replace("latex/", "") # Replace to get the path relative to latex file location

    # Else download it and return the path to it
    response = requests.get(logo_url, stream=True)
    if response.status_code == 200:
        extension = mimetypes.guess_extension(response.headers["Content-Type"])
        extension = ".jpeg" if extension == ".jpe" else extension # BUGFIX for JPEG images that don't get the right extension
        file_path = os.path.join(logos_folder, expected_file_name_base + extension)
        with open(file_path, 'wb') as f:
            response.raw.decode_content = True
            shutil.copyfileobj(response.raw, f)
        return file_path.replace("latex/", "")


def format_time(input_time):
    """
    Shortens the time string and replaces semicolons by "h" symbol, as is standard in France
    :param input_time: time string in two semicolons format
    :param is_start: is False, means that this is the ending time
    :return: time string shortened as much as possible
    """
    # Always remove seconds
    shortened_time = input_time[:-3]

    # Change ":" to "h"
    shortened_time = shortened_time.replace(":", "h")

    # Remove minutes if none
    if shortened_time[-1] == "0" and shortened_time[-2] == "0":
        shortened_time = shortened_time[:-2]

    return shortened_time


def get_missing_days(actual_days):
    missing_days = []
    for day in day_nb_to_name.keys():
        if day not in actual_days:
            missing_days.append(day)
    return missing_days


def is_valid_day_combination(days_combination, days_for_time_interval, time_intervals_for_day):
    # Check if for this combination, all time intervals are the same
    for day in days_combination:
        if day in time_intervals_for_day:
            for time_interval in time_intervals_for_day[day]:
                if not set(days_combination).issubset(set(days_for_time_interval[time_interval])):
                    return False
        else:
            return False
    if len(days_combination) > 0 :
        return True
    else:
        return False


def is_to_textualize_day_combination(days_combination, days_to_textualize):
    for day in days_combination:
        if day not in days_to_textualize:
            return False
    return True


def time_interval_to_text(time_interval):
    return "{starts_at} - {ends_at}".format(starts_at=format_time(time_interval[0]),
                                            ends_at=format_time(time_interval[1]))


def schedule_to_text(schedule_array):
    days_for_time_interval = dict() # {time_tuple: [day_id, day_id, ...]}
    time_intervals_for_day = dict()

    # Properly associate days with time intervals and time intervals with days
    for time_interval in schedule_array:
        time_tuple = (time_interval["begins_at"], time_interval["ends_at"])
        if time_tuple in days_for_time_interval:
            days_for_time_interval[time_tuple].append(time_interval["day"])
        else:
            days_for_time_interval[time_tuple] = [time_interval["day"]]

        if time_interval["day"] in time_intervals_for_day:
            time_intervals_for_day[time_interval["day"]].append(time_tuple)
        else:
            time_intervals_for_day[time_interval["day"]] = [time_tuple]

    # Find all days combinations that have exactly the same schedules
    valid_days_combinations = []
    for days_combination in days_combinations:
        if is_valid_day_combination(days_combination, days_for_time_interval, time_intervals_for_day):
            valid_days_combinations.append(days_combination)
    valid_days_combinations.sort(key=lambda item: item[0])

    days_to_textualize = list(time_intervals_for_day.keys())
    strings_to_join_for_schedule = []

    for days_combination in valid_days_combinations:
        if is_to_textualize_day_combination(days_combination, days_to_textualize):
            common_time_intervals = time_intervals_for_day[days_combination[0]]
            common_time_intervals_as_text = map(time_interval_to_text, common_time_intervals)
            textual_interval = "; ".join(common_time_intervals_as_text)

            days_combination_text = ""

            # Open every day at this time interval
            if len(days_combination) == 7:
                days_combination_text = textual_interval + " tous les jours"
            elif 4 <= len(days_combination) <= 6:
                days_string = ""
                missing_days = get_missing_days(days_combination)
                if len(days_combination) == 6:
                    days_string = day_nb_to_name[missing_days[0]]
                elif len(days_combination) == 5:
                    days_string = "{first_day} et {second_day}".format(
                        first_day=day_nb_to_name[missing_days[0]],
                        second_day=day_nb_to_name[missing_days[1]])
                elif len(days_combination) == 4:
                    days_string = "{first_day}; {second_day} et {third_day}".format(
                        first_day=day_nb_to_name[missing_days[0]],
                        second_day=day_nb_to_name[missing_days[1]],
                        third_day = day_nb_to_name[missing_days[2]])
                days_combination_text = textual_interval + " tous les jours sauf {days}".format(days=days_string)
            elif 1 <= len(days_combination) <= 3:
                days_string = ""
                if len(days_combination) == 3:
                    days_string = "{first_day}; {second_day} et {third_day}".format(
                        first_day=day_nb_to_name[days_combination[0]],
                        second_day=day_nb_to_name[days_combination[1]],
                        third_day=day_nb_to_name[days_combination[2]])
                elif len(days_combination) == 2:
                    days_string = "{first_day} et {second_day}".format(
                        first_day=day_nb_to_name[days_combination[0]],
                        second_day=day_nb_to_name[days_combination[1]])
                elif len(days_combination) == 1:
                    days_string = day_nb_to_name[days_combination[0]]
                days_combination_text = textual_interval + " le {days}".format(days=days_string)

            strings_to_join_for_schedule.append(days_combination_text)
            for day in days_combination:
                days_to_textualize.remove(day)
    schedule_text = ", ".join(strings_to_join_for_schedule)

    return schedule_text


def check_asso_json_validity(asso_data):
    checks = {"is_validated": "is_validated" in asso_data and isinstance(asso_data["is_validated"], (bool,)),
              "is_active": "is_active" in asso_data and isinstance(asso_data["is_active"], (bool,)),
              "category": "category" in asso_data and isinstance(asso_data["category"]["id"], (int,)),
              "name": "name" in asso_data and isinstance(asso_data["name"], (str,)) and asso_data["name"],
              "logo_url": ("logo_url" in asso_data and isinstance(asso_data["logo_url"], (str,))
                           and asso_data["logo_url"] and is_valid_url(asso_data["logo_url"])),
              "schedule": "schedule" in asso_data and isinstance(asso_data["schedule"], (list,)),
              "acronym": "acronym" in asso_data and (isinstance(asso_data["acronym"], (str,)) or asso_data["acronym"] is None),
              "contact_address": ("contact_address" in asso_data and isinstance(asso_data["contact_address"], (str,))
                                  and asso_data["contact_address"] and is_valid_mail(asso_data["contact_address"])),
              "website_url": "website_url" in asso_data and (isinstance(asso_data["website_url"], (str,)) or asso_data["website_url"] is None),
              "description": ("description" in asso_data and isinstance(asso_data["description"], (str,))
                              and 500 < len(asso_data["description"]) <= 900)

    }

    report = ""
    failed_checks_nb = 0
    for criterion, check in checks.items():
        if not check:
            failed_checks_nb += 1
            if failed_checks_nb == 1:
                report += "The association '{name}' could not be added because of missing data:\n".format(
                    name=asso_data["name"])
            report += " - {criterion}\n".format(criterion=criterion)
    if failed_checks_nb > 0:
        print(report)
        return False
    else:
        return True


def tex_escape(text):
    """
        :param text: a plain text message
        :return: the message escaped to appear correctly in LaTeX
    """
    conv = {
        '&': r'\&',
        '%': r'\%',
        '$': r'\$',
        '#': r'\#',
        '_': r'\_',
        '{': r'\{',
        '}': r'\}',
        '~': r'\textasciitilde{}',
        '^': r'\^{}',
        '\\': r'\textbackslash{}',
        '<': r'\textless{}',
        '>': r'\textgreater{}',
    }
    regex = re.compile('|'.join(re.escape(str(key)) for key in sorted(conv.keys(), key = lambda item: - len(item))))
    return regex.sub(lambda match: conv[match.group()], text)


with open('data.json') as json_file:
    # Reset main template tex file from base file
    shutil.copyfile(original_main_template_file_path, main_template_file_path)

    if input("Use data.json ? (y/n) ").lower() == "y":
        assos_data = json.load(json_file) # When using file
    else:
        asso_data_request = requests.get('https://portail.asso-insa-lyon.fr/api/v1/directory/private/?format=json',
                                     auth=(input("Portail VA admin user name: "), getpass.getpass()))
        assos_data = json.loads(asso_data_request.text)

    website_urls_to_check = ["--- Websites URL to check if work:"]

    acronyms_to_check = ["--- Associations acronyms to check if actually meaningful:"]

    schedules_to_check = ["--- Associations schedules to check by calling the associations resps:"]

    assos_names_for_placeholder = dict()

    for category in categories:
        with open(category["latex_filename"], 'w') as latex_file:
            content = template_intro + category_color_text.format(color=category["latex_color"])

            print("Completed content generation for associations :")
            for asso_data in assos_data:
                if (asso_data["is_validated"] and asso_data["is_active"]
                        and asso_data["category"]["id"] in category["botinsa_ids"]):
                    if check_asso_json_validity(asso_data):
                        asso_path_to_logo = get_logo_path(asso_data["name"], asso_data["logo_url"] if asso_data["logo_url"] is not None else "")
                        asso_textual_schedule = schedule_to_text(asso_data["schedule"])

                        if asso_data["website_url"] is None or asso_data["website_url"] == "":
                            if asso_data["facebook_url"] is not None and asso_data["facebook_url"] != "":
                                website_url = asso_data["facebook_url"]
                            elif asso_data["twitter_url"] is not None and asso_data["twitter_url"] != "":
                                website_url = asso_data["twitter_url"]
                            else:
                                website_url = ""
                        else:
                            website_url = asso_data["website_url"]

                        asso_description = tex_escape(asso_data["description"])
                        asso_description = asso_description\
                            .replace("\u00A0", u" ")\
                            .replace(" !", u"\u00A0!")\
                            .replace(" ?", u"\u00A0?")\
                            .replace(" :", u"\u00A0:")
                        asso_description = asso_description.replace('\n', '\n        ')

                        asso_content = template_asso.format(association_acronym=tex_escape(asso_data["acronym"] if asso_data["acronym"] is not None else ""),
                                                            association_name=tex_escape(asso_data["name"]),
                                                            association_mail=asso_data["contact_address"],
                                                            association_website=tex_escape(website_url.replace("facebook.com", "fb.me").replace("www.", "")),
                                                            association_path_to_logo=asso_path_to_logo,
                                                            association_textual_schedule=asso_textual_schedule if asso_textual_schedule != "" else "Pas de permanences.",
                                                            association_description=asso_description)
                        content += asso_content
                        print("- {asso_name}".format(asso_name=asso_data["name"]))
                        placeholder = category["placeholder_for_summary_list"]
                        if placeholder in assos_names_for_placeholder:
                            assos_names_for_placeholder[placeholder].append(tex_escape(asso_data["name"]))
                        else:
                            assos_names_for_placeholder[placeholder] = [tex_escape(asso_data["name"])]

                        # Manual checks to do later
                        website_urls_to_check.append("{asso_name} : {website_url}".format(
                            asso_name=asso_data["name"],
                            website_url=website_url
                        ))
                        if asso_data["acronym"] is not None:
                            acronyms_to_check.append("{asso_name} : {acronym}".format(
                                asso_name=asso_data["name"],
                                acronym=asso_data["acronym"]
                            ))
                        if not asso_data["schedule"]:
                            schedules_to_check.append("{asso_name}".format(
                                asso_name=asso_data["name"]
                            ))

            content += template_ending
            latex_file.write(content)
            latex_file.close()

            with fileinput.FileInput(main_template_file_path, inplace=True, backup='.bak') as file:
                placeholder = category["placeholder_for_summary_list"]
                replacement_text = ", ".join(assos_names_for_placeholder[placeholder])
                for line in file:
                    print(line.replace(placeholder, replacement_text), end='')


    # for element in website_urls_to_check:
    #     print(element)
    # for element in acronyms_to_check:
    #     print(element)
    # for element in schedules_to_check:
    #     print(element)
