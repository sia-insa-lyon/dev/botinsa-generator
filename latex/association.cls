%!TEX root=./animations_et_loisirs.tex

% =========== Warning ! ============
% The documents that will use this class *MUST* be compiled with
% XeLaTeX.
% ==================================

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{association}[03/08/2019 Lelio Chetot]
\LoadClass[handout,9pt]{beamer}

%%% Packages
	\RequirePackage{fontspec}
	\RequirePackage{ragged2e}
	\RequirePackage{pgfpages}
	\RequirePackage{tikz}
		\usetikzlibrary{positioning,calc}
		\RequirePackage{ifthen}
	\RequirePackage{graphicx}
	\RequirePackage{xargs}

%%% Layout setting
	% \pgfpagesuselayout{2 on 1}[a4paper,border shrink=0.5cm]

%%% Commands
	%% Global variables
		\xdef\BotINSA@AssoLogo{default_logo}
		\xdef\BotINSA@AssoMail{}
		\xdef\BotINSA@AssoWeb{}
		\xdef\BotINSA@AssoSchedule{}
		\xdef\BotINSA@CategoryColor{default}

	%% Icons
		\gdef\EnvelopeIcon	{\includegraphics[width=.75em]{logos/envelope.png}}
		\gdef\CloudIcon		{\includegraphics[width=.75em]{logos/cloud.png}}
		\gdef\CalendarIcon	{\includegraphics[width=.75em]{logos/calendar.png}}
		\gdef\MinusIcon		{\includegraphics[width=.75em]{logos/minus.png}}
		\gdef\MinusWhiteIcon{\includegraphics[width=.75em]{logos/minus_white.png}}

	%% Association related commands
		\newcommand\SetAssoWeb[1] 		{\gdef\BotINSA@AssoWeb{#1}}
		\newcommand\SetAssoMail[1]		{\gdef\BotINSA@AssoMail{#1}}
		\newcommand\SetAssoLogo[1]		{\gdef\BotINSA@AssoLogo{#1}}
		\newcommand\SetAssoSchedule[1]	{\gdef\BotINSA@AssoSchedule{#1}}
		\newenvironmentx{association}[6][1={},3=N/A,4=N/A,5=default_logo.png,6=N/A,usedefault]
		{
			\SetAssoMail{#3}
			\SetAssoWeb{#4}
			\SetAssoLogo{#5}
			\SetAssoSchedule{#6}
			\begin{frame}
				\frametitle[#1]{#2}
		}{
			\end{frame}
		}

	%% Category related commands
		\newcommand\SetCategoryColor[1]{\xdef\BotINSA@CategoryColor{#1}}

	%% Miscellaneous
		\newcommandx{\sepcolumn}[1][1=0.005\paperwidth]
		{
			\begin{column}{#1}\end{column}
		}
		\newcommand{\DispIfNotEmpty}[2]
		{
			\ifx#1\@empty%
				%
			\else
				#2
			\fi
		}

%%% Beamer sizes
	\setbeamersize{text margin left=1em}  % <- like this
	\setbeamersize{text margin right=1em} % <- like this

%%% Templates (ordered such as the first one is at the top of the page and the last one at the bottom).
	\setbeamertemplate{background canvas}
	{
		% \begin{beamercolorbox}[wd=\paperwidth,ht=\paperheight]{background canvas}
			\begin{tikzpicture}[remember picture,overlay]
				\draw[
					botinsa-\BotINSA@CategoryColor-vivid,
					line width=2pt,
					fill=white,
					rounded corners
				]
								(current page.north east)
					rectangle 	(current page.south west);
			\end{tikzpicture}
		% \end{beamercolorbox}
	}
	\defbeamertemplate*{headline}{botinsa}{}
	\defbeamertemplate*{frametitle}{botinsa}
	{
		\begin{columns}[T]
			\sepcolumn
			\begin{column}{.8\paperwidth}
				\begin{beamercolorbox}[wd=\textwidth,sep=.1em]{frametitle}
					\centering
					\insertframetitle
					\DispIfNotEmpty{\beamer@shortframetitle}{\\\fs{15}\selectfont\MinusIcon~\insertshortframetitle~\MinusIcon}
				\end{beamercolorbox}
			\end{column}
			\sepcolumn
			\begin{column}{.15\paperwidth}
				\centering
				\includegraphics[width=\textwidth]{\BotINSA@AssoLogo}
			\end{column}
			\sepcolumn
		\end{columns}
	}
	\defbeamertemplate*{footline}{botinsa}
	{%
		\usebeamercolor{footline}
		\begin{tikzpicture}
			\draw[footline.fg] (0,0) node[
				fill=footline.bg,
				minimum width=\paperwidth,
				minimum height=.2\paperheight,
				text width=.9\paperwidth,
				% inner sep=0.05\paperwidth,
				align=left,
				rounded corners
			] at (0,0) {
				\begin{columns}[t]
					\begin{column}{.52\textwidth}
						\baselineskip=20pt
						\EnvelopeIcon~\url{\BotINSA@AssoMail}

						\CloudIcon~\ifthenelse{\equal{\BotINSA@AssoWeb}{N/A}}{Pas de site web}{\url{\BotINSA@AssoWeb}}
					\end{column}
					\sepcolumn[.05\textwidth]
					\begin{column}{.48\textwidth}
						\baselineskip=10pt
						\CalendarIcon~\textbf{\textsc{Permanences}}\\
						\foreach \x in \BotINSA@AssoSchedule {%
							\hskip0em\MinusWhiteIcon~\x\\
						}
					\end{column}
				\end{columns}
			};
		\end{tikzpicture}
	}
	\setbeamertemplate{navigation symbols}{}

%%% Colors
	\include{botinsa-colors}

	%% Map the colors to the page elements (ordered from the page's top to bottom)
		\setbeamercolor{background canvas}	{bg=lightgray}
		\setbeamercolor{headline}			{fg=white,bg=black}
		\setbeamercolor{frametitle}			{fg=black}
		\setbeamercolor{footline}			{fg=white,bg=botinsa-\BotINSA@CategoryColor-vivid}

%%% Fonts
	\setsansfont{Arial}
	\newfontfamily\TitleFont{Impact}

	\newcommand{\fs}[1]{\fontsize{#1}{#1}}

	\setbeamerfont*{frametitle}{family=\TitleFont,size=\fs{18}}
	\setbeamerfont*{footline}{size=\fs{8}}
