# Bot'INSA generator
Outil de génération du Bot'INSA papier à partir des données de [PortailVA](https://portail.asso-insa-lyon.fr) et de templates LaTex.

Pour lancer le script, vous devez posséder des accès administrateur à PortailVA. Pour la génération du pdf à partir des fichiers sources,
il est nécessaire d'installer `XeTeX`, les polices `Arial`, `Impact` et `FontAwesome`, ainsi que les packages tlmgr `fontspec`, `ragged2e`,  `xargs` et`babel-french` (contenus dans l'installation `texlive-full` mais pas `texlive-xetex`).
Chaque catégorie est compilée indépendemment, puis le main.

Référez-vous au [script d'installation](install.sh) pour une installation complète sur Ubuntu.

## Sommaire des templates modifiables
 * [template.tex](latex/template.tex) : Le document maître, qui inclut tous les autres `.tex` générés par le script. C'est aussi ici que sont écrites les pages introductives, de conclusion.
 * [category.sty](latex/category.sty) : Définit la mise en page des catégories d'association
 * [botinsa-colors.tex](latex/botinsa-colors.tex) : Définit les couleurs utilisées pour chaque catégorie d'asso
 * [association.cls](latex/association.cls) : Modèle de présentation d'une association, utilisé pour générer l'encadré de chaque association
 * [backgrounds/](latex/backgrounds) : Intercalaires utilisés pour séparer chaque catégorie d'asso, et fonds. Placez ici les pages entières à inclure
 * [logos/](latex/logos) : Logos et icônes (hors logos d'asso) utilisés dans le document

 ## Infos utiles :
 * Il est possible que certains formats d'images ne marchent pas (ex: .svg), il faut donc modifier le format, et ensuite le nom du fichier dans le data.json
