#!/bin/bash

## Script de compilation du Bot'INSA, testé sous Mint 20
## Devrait fonctionner pareil sous Ubuntu 18 si tlmgr est pas trop relou, changer le repository si besoin


# Installation de Xelatex pour la compilation
sudo apt update
sudo apt upgrade
sudo apt dist-upgrade
sudo apt install texlive-xetex

# Installation des polices Microsoft utilisées
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | sudo debconf-set-selections # Pour ne pas avoir le promp pour l'EULA
sudo apt install ttf-mscorefonts-installer

# Installation de la police fontawesome
wget https://github.com/gabrielelana/awesome-terminal-fonts/raw/master/build/fontawesome-regular.ttf
mv fontawesome-regular.ttf ~/.local/share/fonts
sudo fc-cache -f -v # Mise à jour du cache des polices

# Installation des packages supplémentaires LaTex
tlmgr init-usertree
tlmgr option repository http://ftp.math.utah.edu/pub/tex/historic/systems/texlive/2019/tlnet-final
tlmgr install fontspec ragged2e xargs babel-french --verify-repo=none

# Sur mac depuis l'installation tex de base :
# tlmgr install enumitem blindtext trimspaces environ tcolorbox everypage background titlesec

# Collecte des données PortailVA

# Décommenter si ce n'est pas déjà cloné
# git clone https://gitlab.com/sia-insa-lyon/dev/botinsa-generator
# cd botinsa-generator
python3 botinsa_generator.py

# Compilation latex de chaque catégorie indépendamment
cd latex
xelatex animations_et_loisirs.tex && xelatex arts_et_spectacles.tex  && xelatex cultures_internationales.tex && xelatex departements_et_enseignements.tex && xelatex humanitaire_et_social.tex && xelatex sport.tex && xelatex techniques_et_scientifique.tex

# Compilation du main
xelatex main_output_dont_edit_directly.tex
xelatex main_output_dont_edit_directly.tex # Faut le lancer 2 fois, je sais pas pourquoi...

# Cleanup
mkdir out
mv *.aux *.log *.nav *.out *.snm *.toc *.pdf out/
mv out/main_output_dont_edit_directly.pdf BOTINSA.pdf
